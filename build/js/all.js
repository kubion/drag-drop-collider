/*================================================================
=>                  App = dragDrop
==================================================================*/
/*global angular*/

var app = angular.module('dragDrop', []);


app.config(['$httpProvider', 'dropPlaceholderProvider',
    function($httpProvider, dropPlaceholderProvider) {
        'use strict';

       
        dropPlaceholderProvider.setPositionsElements({
            "top": "[data-row], [data-content]",
            "bottom": "[data-row]:last, [data-content]",
            "left-right": "[data-column]",
        });
    
        dropPlaceholderProvider.setDebugMode(false); 
    
        // This is required for Browser Sync to work poperly
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    }
]);


/*================================================================
=>                  dragDrop App Run()  
==================================================================*/

app.run(['$rootScope',
    function($rootScope) {

        'use strict';

        console.log('Angular.js run() function...');


    }
]);

angular.element(document).ready(function() {
  angular.bootstrap(document, ["dragDrop"]);
});

/* ---> Do not delete this comment (Values) <--- */

/* ---> Do not delete this comment (Constants) <--- */
/*================================================================
=>                  Controller = DropPlaceholderCtrl
==================================================================*/
/*global app*/

app.controller('DropPlaceholderCtrl', ['$scope', 'dropPlaceholder', '$http', '$compile',
    function($scope, dropPlaceholder, $http, $compile) {

        'use strict';

        console.log('Controller ===  DropPlaceholderCtrl');

        $scope.toggleDebug = function($event) {
            dropPlaceholder.debugMode = !dropPlaceholder.debugMode;
            dropPlaceholder.init(dropPlaceholder.debugMode);
        }


        var $row = function() {
            return jQuery("<div class='row' data-row id='" + _.uniqueId("row_") + "' />");
        }

        var $column = function() {
            return jQuery("<div class='columns medium-12' data-column id='" + _.uniqueId("column_") + "' />");
        }

        var $content = function() {
            return jQuery("<div data-content id='" + _.uniqueId("content_") + "' />");
        }

        var redistributeColumns = function() {
            var columnsDefinition = {
                1: 12,
                2: 6,
                3: 4,
                4: 3
            };

            jQuery("[data-row]").each(function(index, row) {
                var $row = jQuery(row);

                var columnsCount = $row.find("[data-column]").length;

                console.log("columnsCount : " + columnsCount);

                $row.find("[data-column]").each(function(index, item) {
                    var newClass = (jQuery(item).attr("class")).toString().replace(/(\w+)\-\d+/, "");
                    jQuery(item).attr("class", newClass);
                }).addClass("medium-" + columnsDefinition[columnsCount]);
            });
        }

        var removeAllElementsWithoutContent = function() {
            String.prototype.isEmpty = function() {
                return (this.length === 0 || !this.trim());
            };

            var checkEmpty = function(element) {
                if (jQuery(element).html().toString().isEmpty()) {
                    jQuery(element).remove();
                    jQuery(".drop-placeholder[data-target-id='" + jQuery(element).attr("id") + "']").remove();
                }
            }

            jQuery("[data-content]")
                .each(function(index, element) {
                    checkEmpty(element);
                });

            jQuery("[data-column]")
                .each(function(index, element) {
                    checkEmpty(element);
                });

            jQuery("[data-row]")
                .each(function(index, element) {
                    checkEmpty(element);
                });
        }

        var rowAsTarget = function(event, data, DOMCommand) {
            if (!data.$draggable.attr("data-template-url")) {
                if (data.$draggable.is("[data-content]")) {
                    jQuery(event.target)[DOMCommand](
                        $row().append(
                            $column().append(data.$draggable)
                        )
                    );
                }

                setTimeout(afterDrop, 100);

            } else {
                $http.get(data.$draggable.attr("data-template-url"))
                    .then(function(repsonse) {
                        jQuery(event.target)[DOMCommand](
                            $row().append(
                                $column().append(
                                    $content().append($compile(repsonse.data)($scope))
                                )
                            )
                        );

                        setTimeout(afterDrop, 100);

                    }, function(err) {
                        console.log(err);
                    });
            }
        }

        var columnAsTarget = function(event, data, DOMCommand) {

            if (!data.$draggable.attr("data-template-url")) {
                if (data.$draggable.is("[data-content]")) {
                    jQuery(event.target)[DOMCommand](
                        $column().append(data.$draggable)
                    );
                }

                setTimeout(afterDrop, 100);

            } else {
                $http.get(data.$draggable.attr("data-template-url"))
                    .then(function(repsonse) {
                        jQuery(event.target)[DOMCommand](
                            $column().append(
                                $content().append(repsonse.data)
                            )
                        );

                        setTimeout(afterDrop, 100);

                    }, function(err) {
                        console.log(err);
                    });
            }
        }

        var coontentAsTarget = function(event, data, DOMCommand) {
            if (!data.$draggable.attr("data-template-url")) {
                if (data.$draggable.is("[data-content]")) {
                    jQuery(event.target)[DOMCommand](
                        $content().append(data.$draggable)
                    );
                }

                setTimeout(afterDrop, 100);

            } else {
                $http.get(data.$draggable.attr("data-template-url"))
                    .then(function(repsonse) {
                        jQuery(event.target)[DOMCommand](
                            $content().append(repsonse.data)
                        );

                        setTimeout(afterDrop, 100);

                    }, function(err) {
                        console.log(err);
                    });
            }
        }

         var afterDrop = function() {
            removeAllElementsWithoutContent();
            redistributeColumns();
            dropPlaceholder.init(dropPlaceholder.debugMode);
        }

        jQuery(document).on("onDrop:top", "[data-row]", function(event, data) {
            event.stopPropagation();
            rowAsTarget(event, data, "before");
        });

        jQuery(document).on("onDrop:bottom", "[data-row]", function(event, data) {
            event.stopPropagation();
            rowAsTarget(event, data, "after");
        });

        jQuery(document).on("onDrop:left", "[data-column]", function(event, data) {
            event.stopPropagation();
            columnAsTarget(event, data, "before");
        });

        jQuery(document).on("onDrop:right", "[data-column]", function(event, data) {
            event.stopPropagation();
            columnAsTarget(event, data, "after");
        });

        jQuery(document).on("onDrop:top", "[data-content]", function(event, data) {
            event.stopPropagation();
            coontentAsTarget(event, data, "before");
        });

        jQuery(document).on("onDrop:bottom", "[data-content]", function(event, data) {
            event.stopPropagation();
            coontentAsTarget(event, data, "after");
        });
    }
]);


/*-----  End of Controller = DropPlaceholderCtrl  ------*/
/*global app*/

var DropPlaceholder = function($dropTarget) {
    this.$dropTarget = $dropTarget;
    this.markerThickness = 2;
    this.spaceWeight = 20;
};

DropPlaceholder.prototype.createVertical = function(position) {
    var $placeholder = jQuery("<div class='drop-placeholder drop-placeholder-vertical' />");
    $placeholder
        .css({
            background: "transparent",
            height: this.$dropTarget.outerHeight() - this.spaceWeight,
            width: this.spaceWeight / (this.markerThickness - 0.5),
            position: "absolute"
        })
        .attr("data-target-id", this.$dropTarget.attr("id"))
        .attr("data-position", position);

    var $marker = jQuery("<div class='marker' />")
        .css({
            width: this.markerThickness,
            height: this.$dropTarget.outerHeight() - this.spaceWeight,
            display: "none",
        });

    if (position === "left") {
        $marker.css({
            "margin-left": $placeholder.outerWidth()
        });
    }

    $placeholder
        .append($marker)
        .prependTo("body");

    this.setPosition($placeholder, position);

};

DropPlaceholder.prototype.createHorizontal = function(position) {
    var $placeholder = jQuery("<div class='drop-placeholder drop-placeholder-horizontal' />");
    $placeholder
        .css({
            background: "transparent",
            height: this.spaceWeight,
            width: this.$dropTarget.outerWidth(),
            position: "absolute"
        })
        .attr("data-target-id", this.$dropTarget.attr("id"))
        .attr("data-position", position);

    var $marker = jQuery("<div class='marker' />")
        .css({
            height: this.markerThickness,
            display: "none",
            "margin-top": this.spaceWeight / 2
        });

    $placeholder
        .append($marker)
        .prependTo("body");

    this.setPosition($placeholder, position);

};

DropPlaceholder.prototype.drawAtPosition = function(position) {
    if (position === "top" || position === "bottom") {
        this.createHorizontal(position);
    } else if (position === "left" || position === "right") {
        this.createVertical(position);
    }
};

DropPlaceholder.prototype.setPosition = function($placeholder, position) {
    var my, at;

    if (position === "top" || position === "bottom") {
        my = "left center";
        at = "left " + position;
    } else if (position === "right") {
        my = "left top+" + this.spaceWeight / 2;
        at = "right top";
    } else if (position === "left") {
        my = "left-" + $placeholder.outerWidth() + " top+" + this.spaceWeight / 2;
        at = "left top";
    }

    $placeholder
        .position({
            my: my,
            at: at,
            of: this.$dropTarget,
            collision: "none"
        });
};

DropPlaceholder.prototype.setDebug = function(isDebugMode) {
    if (isDebugMode) {
        jQuery(".drop-placeholder").each(function(index, placeholder) {
            jQuery(placeholder).css({
                background: "red",
                opacity: 0.4
            });
            jQuery(placeholder).find(".marker")
                .addClass("debug")
                .css({
                    display: "block"
                });
        });
    } else {
        jQuery(".drop-placeholder").each(function(index, placeholder) {
            jQuery(placeholder).css({
                background: "transparent",
            });
            jQuery(placeholder).find(".marker")
                .removeClass("debug")
                .css({
                    display: "none"
                });
        });
    }
};

var DropPlaceholdersGenerator = function(elementAndPositions) {
    this.elementAndPositions = elementAndPositions;
};

DropPlaceholdersGenerator.prototype.create = function(isDebugMode) {
    var self = this;
    jQuery(".drop-placeholder").remove();

    var make = function(positions) {
        positions.split("-").forEach(function(position) {
            var $elements = jQuery(self.elementAndPositions[positions])
            $elements.each(function(index, element) {
                var dropPlaceholder = new DropPlaceholder(jQuery(element));
                dropPlaceholder.drawAtPosition(position);
                dropPlaceholder.setDebug(isDebugMode);
            });
        });
    };

    for (var positions in this.elementAndPositions) {
        make(positions);
    }

    jQuery(".drop-placeholder").droppable({
        hoverClass: "_hover",
        tolerance: "pointer",
        greedy: true,
        drop: function(event, ui) {
            var $draggableHelper = jQuery(ui.helper);
            var $draggable = jQuery(ui.draggable);
            var droppableTargetId = jQuery(this).data("targetId");
            var $droppableTarget = jQuery("#" + droppableTargetId);
            var position = jQuery(this).data("position");

            console.log("dropTarget: " + droppableTargetId + ", position: " + position);

            $droppableTarget.trigger("onDrop:" + position, {
                $draggableHelper: $draggableHelper,
                $draggable: $draggable,
                droppableTargetId: droppableTargetId,
                $droppableTarget: $droppableTarget,
                position: position
            });
        }
    });
};

app.provider('dropPlaceholder', function() {

    'use strict';

    this.debugMode = false;

    this.$get = function() {

        var self = this;

        var init = function(debugMode) {
            initDraggables();
            var placeholders = new DropPlaceholdersGenerator(self.positionsElements);
            placeholders.create(debugMode);
        }

        var initDraggables = function() {
            jQuery("[data-draggable]").draggable({
                opacity: 0.6,
                zIndex: 1,
                revert: "invalid",
                cursor: "move",
                helper: function() {
                    return  $(this).clone();
                    // return $(this).clone().css("pointer-events","none").appendTo("body").show();
                }
            });

            jQuery("[data-content]").draggable({
                opacity: 0.6,
                zIndex: 1,
                revert: "invalid",
                cursor: "move",
                helper: function(event) {
                    return jQuery(jQuery(this).html()).css("pointer-events","none").appendTo("body").show();
                    // return jQuery(this).clone().removeClass("_hover").removeClass("th")
                }
            })
        }

        initDraggables();


        jQuery(document).on("mouseover", "[data-content], [data-column]", function(event) {
            event.stopPropagation();
            jQuery(this).removeClass("_hover");
            jQuery(this).addClass("_hover");
        });

        jQuery(document).on("mouseout", "[data-content], [data-column]", function(event) {
            event.stopPropagation();
            jQuery(this).removeClass("_hover");
        });

        setTimeout(function() {
            jQuery("body").prepend('<div id="ghost-dropppable" />');

            jQuery("#ghost-dropppable").droppable({
                activate: function(event) {
                    console.log(event.type);
                    init(self.debugMode)
                }
            });
        }, 100);

        return {
            init: init
        }
    };

    this.setDebugMode = function(debugMode) {
        this.debugMode = debugMode;
    }

    this.setPositionsElements = function(positionsElements) {
        this.positionsElements = positionsElements;
    };
});
/*================================================================
=>                  Directive = imageComponent
==================================================================*/
/*global app*/

app.directive('imageComponent', ['$rootScope',
    function($rootScope) {

        'use strict';

        return {
            restrict: 'A',
            scope: {

            },
            link: function(scope, element, attrs) {
                console.log('Directive === imageComponent');


                scope.title = Math.random() * 10000;

                scope.getRandomImageUrl = function() {
                    return "http://lorempixel.com/g/400/200/?random=" + Math.random();
                }
            }
        };
    }
])
    .controller('ImageCtrl', ['$scope',
        function($scope) {

            'use strict';

            console.log('Controller ===  ImageCtrl');


        }
    ]);


/*-----  End of Directive = imageComponent  ------*/
/*================================================================
=>                  Directive = imageComponent
==================================================================*/
/*global app*/

app.directive('imageComponent', ['$rootScope',
    function($rootScope) {

        'use strict';

        return {
            restrict: 'A',
            scope: {

            },
            link: function(scope, element, attrs) {
                console.log('Directive === imageComponent');


                scope.title = Math.random() * 10000;

                scope.getRandomImageUrl = function() {
                    return "http://lorempixel.com/g/400/200/?random=" + Math.random();
                }
            }
        };
    }
])
    .controller('ImageCtrl', ['$scope',
        function($scope) {

            'use strict';

            console.log('Controller ===  ImageCtrl');


        }
    ]);


/*-----  End of Directive = imageComponent  ------*/
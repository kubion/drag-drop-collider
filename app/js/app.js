/*================================================================
=>                  App = dragDrop
==================================================================*/
/*global angular*/

var app = angular.module('dragDrop', []);


app.config(['$httpProvider', 'dropPlaceholderProvider',
    function($httpProvider, dropPlaceholderProvider) {
        'use strict';

       
        dropPlaceholderProvider.setPositionsElements({
            "top": "[data-row], [data-content]",
            "bottom": "[data-row]:last, [data-content]",
            "left-right": "[data-column]",
        });
    
        dropPlaceholderProvider.setDebugMode(false); 
    
        // This is required for Browser Sync to work poperly
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    }
]);


/*================================================================
=>                  dragDrop App Run()  
==================================================================*/

app.run(['$rootScope',
    function($rootScope) {

        'use strict';

        console.log('Angular.js run() function...');


    }
]);

angular.element(document).ready(function() {
  angular.bootstrap(document, ["dragDrop"]);
});

/* ---> Do not delete this comment (Values) <--- */

/* ---> Do not delete this comment (Constants) <--- */
/*================================================================
=>                  Controller = DropPlaceholderCtrl
==================================================================*/
/*global app*/

app.controller('DropPlaceholderCtrl', ['$scope', 'dropPlaceholder', '$http', '$compile',
    function($scope, dropPlaceholder, $http, $compile) {

        'use strict';

        console.log('Controller ===  DropPlaceholderCtrl');

        $scope.toggleDebug = function($event) {
            dropPlaceholder.debugMode = !dropPlaceholder.debugMode;
            dropPlaceholder.init(dropPlaceholder.debugMode);
        }


        var $row = function() {
            return jQuery("<div class='row' data-row id='" + _.uniqueId("row_") + "' />");
        }

        var $column = function() {
            return jQuery("<div class='columns medium-12' data-column id='" + _.uniqueId("column_") + "' />");
        }

        var $content = function() {
            return jQuery("<div data-content id='" + _.uniqueId("content_") + "' />");
        }

        var redistributeColumns = function() {
            var columnsDefinition = {
                1: 12,
                2: 6,
                3: 4,
                4: 3
            };

            jQuery("[data-row]").each(function(index, row) {
                var $row = jQuery(row);

                var columnsCount = $row.find("[data-column]").length;

                console.log("columnsCount : " + columnsCount);

                $row.find("[data-column]").each(function(index, item) {
                    var newClass = (jQuery(item).attr("class")).toString().replace(/(\w+)\-\d+/, "");
                    jQuery(item).attr("class", newClass);
                }).addClass("medium-" + columnsDefinition[columnsCount]);
            });
        }

        var removeAllElementsWithoutContent = function() {
            String.prototype.isEmpty = function() {
                return (this.length === 0 || !this.trim());
            };

            var checkEmpty = function(element) {
                if (jQuery(element).html().toString().isEmpty()) {
                    jQuery(element).remove();
                    jQuery(".drop-placeholder[data-target-id='" + jQuery(element).attr("id") + "']").remove();
                }
            }

            jQuery("[data-content]")
                .each(function(index, element) {
                    checkEmpty(element);
                });

            jQuery("[data-column]")
                .each(function(index, element) {
                    checkEmpty(element);
                });

            jQuery("[data-row]")
                .each(function(index, element) {
                    checkEmpty(element);
                });
        }

        var rowAsTarget = function(event, data, DOMCommand) {
            if (!data.$draggable.attr("data-template-url")) {
                if (data.$draggable.is("[data-content]")) {
                    jQuery(event.target)[DOMCommand](
                        $row().append(
                            $column().append(data.$draggable)
                        )
                    );
                }

                setTimeout(afterDrop, 100);

            } else {
                $http.get(data.$draggable.attr("data-template-url"))
                    .then(function(repsonse) {
                        jQuery(event.target)[DOMCommand](
                            $row().append(
                                $column().append(
                                    $content().append($compile(repsonse.data)($scope))
                                )
                            )
                        );

                        setTimeout(afterDrop, 100);

                    }, function(err) {
                        console.log(err);
                    });
            }
        }

        var columnAsTarget = function(event, data, DOMCommand) {

            if (!data.$draggable.attr("data-template-url")) {
                if (data.$draggable.is("[data-content]")) {
                    jQuery(event.target)[DOMCommand](
                        $column().append(data.$draggable)
                    );
                }

                setTimeout(afterDrop, 100);

            } else {
                $http.get(data.$draggable.attr("data-template-url"))
                    .then(function(repsonse) {
                        jQuery(event.target)[DOMCommand](
                            $column().append(
                                $content().append(repsonse.data)
                            )
                        );

                        setTimeout(afterDrop, 100);

                    }, function(err) {
                        console.log(err);
                    });
            }
        }

        var coontentAsTarget = function(event, data, DOMCommand) {
            if (!data.$draggable.attr("data-template-url")) {
                if (data.$draggable.is("[data-content]")) {
                    jQuery(event.target)[DOMCommand](
                        $content().append(data.$draggable)
                    );
                }

                setTimeout(afterDrop, 100);

            } else {
                $http.get(data.$draggable.attr("data-template-url"))
                    .then(function(repsonse) {
                        jQuery(event.target)[DOMCommand](
                            $content().append(repsonse.data)
                        );

                        setTimeout(afterDrop, 100);

                    }, function(err) {
                        console.log(err);
                    });
            }
        }

         var afterDrop = function() {
            removeAllElementsWithoutContent();
            redistributeColumns();
            dropPlaceholder.init(dropPlaceholder.debugMode);
        }

        jQuery(document).on("onDrop:top", "[data-row]", function(event, data) {
            event.stopPropagation();
            rowAsTarget(event, data, "before");
        });

        jQuery(document).on("onDrop:bottom", "[data-row]", function(event, data) {
            event.stopPropagation();
            rowAsTarget(event, data, "after");
        });

        jQuery(document).on("onDrop:left", "[data-column]", function(event, data) {
            event.stopPropagation();
            columnAsTarget(event, data, "before");
        });

        jQuery(document).on("onDrop:right", "[data-column]", function(event, data) {
            event.stopPropagation();
            columnAsTarget(event, data, "after");
        });

        jQuery(document).on("onDrop:top", "[data-content]", function(event, data) {
            event.stopPropagation();
            coontentAsTarget(event, data, "before");
        });

        jQuery(document).on("onDrop:bottom", "[data-content]", function(event, data) {
            event.stopPropagation();
            coontentAsTarget(event, data, "after");
        });
    }
]);


/*-----  End of Controller = DropPlaceholderCtrl  ------*/
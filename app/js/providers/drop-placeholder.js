/*global app*/

var DropPlaceholder = function($dropTarget) {
    this.$dropTarget = $dropTarget;
    this.markerThickness = 2;
    this.spaceWeight = 20;
};

DropPlaceholder.prototype.createVertical = function(position) {
    var $placeholder = jQuery("<div class='drop-placeholder drop-placeholder-vertical' />");
    $placeholder
        .css({
            background: "transparent",
            height: this.$dropTarget.outerHeight() - this.spaceWeight,
            width: this.spaceWeight / (this.markerThickness - 0.5),
            position: "absolute"
        })
        .attr("data-target-id", this.$dropTarget.attr("id"))
        .attr("data-position", position);

    var $marker = jQuery("<div class='marker' />")
        .css({
            width: this.markerThickness,
            height: this.$dropTarget.outerHeight() - this.spaceWeight,
            display: "none",
        });

    if (position === "left") {
        $marker.css({
            "margin-left": $placeholder.outerWidth()
        });
    }

    $placeholder
        .append($marker)
        .prependTo("body");

    this.setPosition($placeholder, position);

};

DropPlaceholder.prototype.createHorizontal = function(position) {
    var $placeholder = jQuery("<div class='drop-placeholder drop-placeholder-horizontal' />");
    $placeholder
        .css({
            background: "transparent",
            height: this.spaceWeight,
            width: this.$dropTarget.outerWidth(),
            position: "absolute"
        })
        .attr("data-target-id", this.$dropTarget.attr("id"))
        .attr("data-position", position);

    var $marker = jQuery("<div class='marker' />")
        .css({
            height: this.markerThickness,
            display: "none",
            "margin-top": this.spaceWeight / 2
        });

    $placeholder
        .append($marker)
        .prependTo("body");

    this.setPosition($placeholder, position);

};

DropPlaceholder.prototype.drawAtPosition = function(position) {
    if (position === "top" || position === "bottom") {
        this.createHorizontal(position);
    } else if (position === "left" || position === "right") {
        this.createVertical(position);
    }
};

DropPlaceholder.prototype.setPosition = function($placeholder, position) {
    var my, at;

    if (position === "top" || position === "bottom") {
        my = "left center";
        at = "left " + position;
    } else if (position === "right") {
        my = "left top+" + this.spaceWeight / 2;
        at = "right top";
    } else if (position === "left") {
        my = "left-" + $placeholder.outerWidth() + " top+" + this.spaceWeight / 2;
        at = "left top";
    }

    $placeholder
        .position({
            my: my,
            at: at,
            of: this.$dropTarget,
            collision: "none"
        });
};

DropPlaceholder.prototype.setDebug = function(isDebugMode) {
    if (isDebugMode) {
        jQuery(".drop-placeholder").each(function(index, placeholder) {
            jQuery(placeholder).css({
                background: "red",
                opacity: 0.4
            });
            jQuery(placeholder).find(".marker")
                .addClass("debug")
                .css({
                    display: "block"
                });
        });
    } else {
        jQuery(".drop-placeholder").each(function(index, placeholder) {
            jQuery(placeholder).css({
                background: "transparent",
            });
            jQuery(placeholder).find(".marker")
                .removeClass("debug")
                .css({
                    display: "none"
                });
        });
    }
};

var DropPlaceholdersGenerator = function(elementAndPositions) {
    this.elementAndPositions = elementAndPositions;
};

DropPlaceholdersGenerator.prototype.create = function(isDebugMode) {
    var self = this;
    jQuery(".drop-placeholder").remove();

    var make = function(positions) {
        positions.split("-").forEach(function(position) {
            var $elements = jQuery(self.elementAndPositions[positions])
            $elements.each(function(index, element) {
                var dropPlaceholder = new DropPlaceholder(jQuery(element));
                dropPlaceholder.drawAtPosition(position);
                dropPlaceholder.setDebug(isDebugMode);
            });
        });
    };

    for (var positions in this.elementAndPositions) {
        make(positions);
    }

    jQuery(".drop-placeholder").droppable({
        hoverClass: "_hover",
        tolerance: "pointer",
        greedy: true,
        drop: function(event, ui) {
            var $draggableHelper = jQuery(ui.helper);
            var $draggable = jQuery(ui.draggable);
            var droppableTargetId = jQuery(this).data("targetId");
            var $droppableTarget = jQuery("#" + droppableTargetId);
            var position = jQuery(this).data("position");

            console.log("dropTarget: " + droppableTargetId + ", position: " + position);

            $droppableTarget.trigger("onDrop:" + position, {
                $draggableHelper: $draggableHelper,
                $draggable: $draggable,
                droppableTargetId: droppableTargetId,
                $droppableTarget: $droppableTarget,
                position: position
            });
        }
    });
};

app.provider('dropPlaceholder', function() {

    'use strict';

    this.debugMode = false;

    this.$get = function() {

        var self = this;

        var init = function(debugMode) {
            initDraggables();
            var placeholders = new DropPlaceholdersGenerator(self.positionsElements);
            placeholders.create(debugMode);
        }

        var initDraggables = function() {
            jQuery("[data-draggable]").draggable({
                opacity: 0.6,
                zIndex: 1,
                revert: "invalid",
                cursor: "move",
                helper: function() {
                    return  $(this).clone();
                    // return $(this).clone().css("pointer-events","none").appendTo("body").show();
                }
            });

            jQuery("[data-content]").draggable({
                opacity: 0.6,
                zIndex: 1,
                revert: "invalid",
                cursor: "move",
                helper: function(event) {
                    return jQuery(jQuery(this).html()).css("pointer-events","none").appendTo("body").show();
                    // return jQuery(this).clone().removeClass("_hover").removeClass("th")
                }
            })
        }

        initDraggables();


        jQuery(document).on("mouseover", "[data-content], [data-column]", function(event) {
            event.stopPropagation();
            jQuery(this).removeClass("_hover");
            jQuery(this).addClass("_hover");
        });

        jQuery(document).on("mouseout", "[data-content], [data-column]", function(event) {
            event.stopPropagation();
            jQuery(this).removeClass("_hover");
        });

        setTimeout(function() {
            jQuery("body").prepend('<div id="ghost-dropppable" />');

            jQuery("#ghost-dropppable").droppable({
                activate: function(event) {
                    console.log(event.type);
                    init(self.debugMode)
                }
            });
        }, 100);

        return {
            init: init
        }
    };

    this.setDebugMode = function(debugMode) {
        this.debugMode = debugMode;
    }

    this.setPositionsElements = function(positionsElements) {
        this.positionsElements = positionsElements;
    };
});